<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'reference' => $this->faker->word,
            'name' => $this->faker->lastName,
            'description' => $this->faker->paragraph,
            'price' => $this->faker->numberBetween(1000, 10000),
            'sold' => $this->faker->randomNumber(2),
            'idBrand' => (\App\Models\Brand::inRandomOrder()->first())->id,
            'imageAdresse' => $this->faker->text,
        ];
    }
    
    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Product $product) {
            $sizes = ['XS', 'S' , 'M', 'L', 'XL', 'XXL'];
            $colors = ['Blue', 'Yellow', 'Red', 'Green', 'Orange'];
            $randomNumber = $this->faker->numberBetween(2, 4);
            $subCategories = \App\Models\SubCategory::inRandomOrder()->paginate($randomNumber);
            for ($i=0; $i < $randomNumber; $i++) {
                $size = $sizes[$this->faker->numberBetween(0, count($sizes)-1)];
                $color = $colors[$this->faker->numberBetween(0, count($colors)-1)];
                $imageAdresse = '/images/tshirts/tshirt_' . $color.'.jpg';
                $json = ["size" => $size, "color" => $color];
                \App\Models\ProductSubCategory::create([
                    'idProduct' => $product->id,
                    'idSubCategory' => $subCategories[$i]->id,
                ]);
                \App\Models\ProductVariation::create([
                    'idProduct' => $product->id,
                    'variations' => $json,
                    'stock' => $this->faker->randomNumber(1),
                    'imageAdresse' => $imageAdresse,
                ]);
            }
        });
    }
}
