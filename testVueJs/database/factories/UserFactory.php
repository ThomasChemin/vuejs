<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->lastName,
            'firstName' => $this->faker->firstName,
            'email' => $this->faker->unique()->safeEmail,
            'adresse' => $this->faker->address,
            'city' => $this->faker->city,
            'cp' => $this->faker->postcode,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }
    
    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (User $user) {
                    \App\Models\UserRole::create([
                        'idUser' => $user->id,
                        'idRole' => '3',
                    ]);
                    \App\Models\Basket::create([
                        'idUser' => $user->id,
                    ]);
        });
    }
}
