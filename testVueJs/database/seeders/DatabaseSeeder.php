<?php

namespace Database\Seeders;

use App\Models\ProductVariation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        \App\Models\Role::factory(1)->create();
        \App\Models\User::factory(10)->create();
        \App\Models\Category::factory(10)->create();
        \App\Models\SubCategory::factory(25)->create();
        \App\Models\Brand::factory(25)->create();
        \App\Models\Product::factory(100)->create();
        $this->removeDuplicateVariations();
    }

    public function removeDuplicateVariations(){
        $nbVariationsDuplicated = DB::table('product_variation')->select('idProduct', 'variations')->groupBy('idProduct', 'variations')->havingRaw('COUNT(idProduct) > 1')->get();
        foreach($nbVariationsDuplicated as $duplicated){
            ProductVariation::where('idProduct', '=', $duplicated->idProduct)->where('variations', '=', $duplicated->variations)->delete();
        }
    }

}
