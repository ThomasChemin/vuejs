/*DELETE DUPLICATE VARIATIONS FROM PRODUCT*/
DELETE FROM product_variation WHERE id = (SELECT id FROM product_variation GROUP BY idProduct, variations HAVING COUNT(idProduct) > 1 LIMIT 1);
