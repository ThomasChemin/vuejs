<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasketProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basket_products', function (Blueprint $table) {
            $table->integer('idBasket');
            $table->integer('idProductVariation');
            $table->integer('quantity');
            $table->foreign('idBasket')->references('id')->on('basket');
            $table->foreign('idProductVariation')->references('id')->on('product_variation');
            $table->primary(['idBasket', 'idProductVariation']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basket_products');
    }
}
