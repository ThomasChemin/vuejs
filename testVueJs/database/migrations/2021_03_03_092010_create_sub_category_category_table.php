<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCategoryCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_category_category', function (Blueprint $table) {
            $table->integer('idCategory');
            $table->integer('idSubCategory');
            $table->primary(['idCategory', 'idSubCategory']);
            $table->foreign('idCategory')->references('id')->on('category');
            $table->foreign('idSubCategory')->references('id')->on('sub_category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_category_category');
    }
}
