<?php

use App\Http\Controllers\Api\BasketProductController;
use App\Http\Controllers\Api\BillController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\SubCategoryController;
use App\Http\Controllers\Api\OptionVariationController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\WishlistController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('/')
    ->group(function () {
        Route::get('/home', [HomeController::class, 'index'])->name('home');
        Route::get('/', [HomeController::class, 'showWelcome'])->name('welcome');
    });

Route::any('/{slug}', function () {
    return view('welcome');
});


Route::prefix('api')
    ->group(function () {
        Route::get('/allUsers', [UserController::class, 'index'])->name('allUsers');
        Route::get('/allCategories', [CategoryController::class, 'index'])->name('allCategories');
        Route::get('/allSubCategories', [SubCategoryController::class, 'index'])->name('allSubCategories');
        Route::prefix('mail')
            ->group(function () {
                Route::post('/send', [UserController::class, 'sendMail'])->name('sendMail');
            });
        Route::prefix('product')
            ->group(function () {
                Route::get('/all', [ProductController::class, 'index'])->name('allProducts');
                Route::get('/allFromCategory/{categoryId}/{subCategoryId}', [ProductController::class, 'allProductFromCategory'])->name('allProductFromCategory');
                Route::get('/find/{prodId}', [ProductController::class, 'find'])->name('findProductById');
                Route::get('/getProductsStartingBy/{name?}', [ProductController::class, 'startingBy'])->name('findProductStartingBy');
            });
        Route::middleware('auth')
            ->prefix('basketProduct')
            ->group(function () {
                Route::get('/getAll', [BasketProductController::class, 'getAll'])->name('getAllBasketProduct');
                Route::get('/get/{idBasket}', [BasketProductController::class, 'getById'])->name('getBasketProductById');
                Route::get('/getAllFromUser', [BasketProductController::class, 'getAllFromUser'])->name('getAllBasketProductFromUser');
                Route::post('/add', [BasketProductController::class, 'addProductToBasket'])->name('addProductToBasket');
                Route::post('/update', [BasketProductController::class, 'update'])->name('updateProductBasket');
                Route::post('/delete', [BasketProductController::class, 'delete'])->name('deleteProductBasket');
            });
        Route::middleware('auth')
            ->prefix('bill')
            ->group(function () {
                Route::get('/countries', [BillController::class, 'retrieveCountriesForStripe'])->name('getCountries');
                Route::get('/getAllFromUser/{idUser}', [BillController::class, 'getAllFromUser'])->name('getAllBillFromUser');
                Route::post('/add', [BillController::class, 'add'])->name('addBill');
            });
        Route::middleware('auth')
            ->prefix('wishlist')
            ->group(function () {
                Route::get('/getAll', [WishlistController::class, 'getAllFromUser'])->name('getAllWishlistFromUser');
                Route::get('/getAllPaginateTwelve', [WishlistController::class, 'getAllFromUserPaginateTwelve'])->name('getAllWishlistFromUserPaginateTwelve');
                Route::get('/getOneProduct/{productId}', [WishlistController::class, 'getFromProductIdForUser'])->name('getFromProductIdForUser');
                Route::post('/add', [WishlistController::class, 'add'])->name('addProductToWishlist');
                Route::post('/remove', [WishlistController::class, 'remove'])->name('remvoeProductFromWishlist');
            });
        Route::prefix('comment')
            ->group(function () {
                Route::get('/getAll', [CommentController::class, 'getAll'])->name('getAllComment');
                Route::get('/get/{commentId}', [CommentController::class, 'get'])->name('getComment');
                Route::get('/getAllFromUser', [CommentController::class, 'getAllFromUser'])->name('getAllCommentFromUser');
                Route::get('/getAllFromProduct/{productId}', [CommentController::class, 'getAllFromProduct'])->name('getAllCommentFromProduct');
                Route::get('/getAllFromProductWithUser/{productId}', [CommentController::class, 'getAllFromProductWithUser'])->name('getAllCommentFromProductWithUser');
                Route::post('/add', [CommentController::class, 'add'])->name('addComment');
                Route::post('/remove', [CommentController::class, 'remove'])->name('removeComment');
            });
    });
