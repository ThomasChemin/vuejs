<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BasketProduct extends Model {

    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'basket_products';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = ['idBasket', 'idProductVariation'];

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * Get the primary Key from his name
     * @return type
     */
    public function getKey() {
        $attributes = [];
        foreach ($this->getKeyName() as $key) {
            $attributes[$key] = $this->getAttribute($key);
        }

        return $attributes;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idBasket',
        'idProductVariation',
        'quantity',
    ];

    /**
     * Relation between ProductVariation and BasketProduct
     * @return type
     */
    public function productVariation() {
        return $this->belongsTo(ProductVariation::class, 'idProductVariation', 'id');
    }

    /**
     * Relation between Basket and BasketProduct
     * @return type
     */
    public function basket() {
        return $this->belongsTo(Basket::class, 'idBasket', 'id');
    }

}
