<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;

class User extends Authenticatable {

    use HasFactory,
        Notifiable,
        Billable;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'users';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = "id";

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = true;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'firstName',
        'email',
        'adresse',
        'city',
        'cp',
        'password',
        'stripe_id',
        'card_brand',
        'card_last_four',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Relation between User and UserRole
     * @return type
     */
    public function userRole() {
        return $this->hasMany(UserRole::class, 'idUser', 'id');
    }

    /**
     * Relation between User and Basket
     * @return type
     */
    public function baskets() {
        return $this->hasMany(Basket::class, 'idUser', 'id');
    }

}
