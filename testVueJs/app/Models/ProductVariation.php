<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'product_variation';

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = true;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = 'id';

    /**
     * Cast the JSON into an array
     */
    protected $casts = [
        'variations' => 'array',
    ];

    /**
     * Attributes that are not able be fillable
     * @var type 
     */
    protected $guarded = [];

    /**
     * Relation between optionValue and value
     * @return type
     */
    public function product() {
        return $this->belongsTo(Product::class, 'idProduct', 'id');
    }
}
