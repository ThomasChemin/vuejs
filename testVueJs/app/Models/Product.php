<?php

namespace App\Models;

use App\Http\Resources\BrandResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'product';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = "id";

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = true;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'reference',
        'description',
        'price',
        'idBrand',
        'imageAdresse',
    ];

    /**
     * Relation between Product and ProductSubCategory
     * @return type
     */
    public function subCategories() {
        return $this->hasMany(ProductSubCategory::class, 'idProduct', 'id');
    }

    /**
     * Relation between Brand and Product
     * @return type
     */
    public function brand() {
        return $this->belongsTo(Brand::class, 'idBrand', 'id');
    }

    /**
     * Relation between Product and BasketProduct
     * @return type
     */
    public function basketProducts() {
        return $this->hasMany(BasketProduct::class, 'idProduct', 'id');
    }

    /**
     * Relation between Product and ProductOption
     * @return type
     */
    public function productVariations() {
        return $this->hasMany(ProductVariation::class, 'idProduct', 'id');
    }

}
