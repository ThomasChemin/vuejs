<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'comment';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = "id";

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = true;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'idUser',
        'idProduct',
        'comment',
    ];

    /**
     * Relation between Product and ProductSubCategory
     * @return type
     */
    public function user() {
        return $this->belongsTo(User::class, 'idUser', 'id');
    }

    /**
     * Relation between Brand and Product
     * @return type
     */
    public function product() {
        return $this->belongsTo(Product::class, 'idProduct', 'id');
    }
}
