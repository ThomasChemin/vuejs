<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'bill';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = "id";

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = true;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUser',
        'price',
        'adresse',
        'idBasketCompleted',
    ];

    /**
     * Relation between Bill and User
     * @return type
     */
    public function user() {
        return $this->belongsTo(User::class, 'idUser', 'id');
    }

    /**
     * Relation between Bill and Basket
     * @return type
     */
    public function basket() {
        return $this->belongsTo(Basket::class, 'idBasket', 'id');
    }
}
