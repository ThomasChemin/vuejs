<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    use HasFactory;
    
    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'product_sub_category';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = ['idProduct', 'idSubCategory'];

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * Get the primary Key from his name
     * @return type
     */
    public function getKey() {
        $attributes = [];
        foreach ($this->getKeyName() as $key) {
            $attributes[$key] = $this->getAttribute($key);
        }

        return $attributes;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idProduct',
        'idSubCategory',
    ];

    /**
     * Relation between Product and SubCategoryCategory
     * @return type
     */
    public function product() {
        return $this->belongsTo(Product::class, 'idProduct', 'id');
    }
    
    /**
     * Relation between SubCategory and SubCategoryCategory
     * @return type
     */
    public function subCategory() {
        return $this->belongsTo(SubCategory::class, 'idSubCategory', 'id');
    }

}
