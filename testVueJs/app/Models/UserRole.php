<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model {

    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'user_role';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = ['idRole', 'idUser'];

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * Get the primary Key from his name
     * @return type
     */
    public function getKey() {
        $attributes = [];
        foreach ($this->getKeyName() as $key) {
            $attributes[$key] = $this->getAttribute($key);
        }

        return $attributes;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUser',
        'idRole',
    ];

    /**
     * Relation between Role and UserRole
     * @return type
     */
    public function role() {
        return $this->belongsTo(Role::class, 'id', 'idRole');
    }

    /**
     * Relation between User and UserRole
     * @return type
     */
    public function user() {
        return $this->belongsTo(User::class, 'id', 'idUser');
    }

}
