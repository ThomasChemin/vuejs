<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'wishlist';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = ['idProduct', 'idUser'];

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * Get the primary Key from his name
     * @return type
     */
    public function getKey() {
        $attributes = [];
        foreach ($this->getKeyName() as $key) {
            $attributes[$key] = $this->getAttribute($key);
        }

        return $attributes;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUser',
        'idProduct',
    ];

    /**
     * Relation between Role and UserRole
     * @return type
     */
    public function product() {
        return $this->belongsTo(Product::class, 'idProduct', 'id');
    }

    /**
     * Relation between User and UserRole
     * @return type
     */
    public function user() {
        return $this->belongsTo(User::class, 'idUser', 'id');
    }
}
