<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model {

    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'basket';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = "id";

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = true;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUser',
        'idRole',
    ];

    /**
     * Relation between Basket and BasketProduct
     * @return type
     */
    public function basketProducts() {
        return $this->hasMany(BasketProduct::class, 'idBasket', 'id');
    }

    /**
     * Relation between User and Basket
     * @return type
     */
    public function user() {
        return $this->belongsTo(User::class, 'id', 'idUser');
    }
}
