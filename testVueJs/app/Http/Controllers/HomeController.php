<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->to('/accueil');
    }

    /**
     * Show the application first page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showWelcome(){
        return redirect()->to('/accueil');
    }
}
