<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Mail\MailSupport;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function index(){
        return UserResource::collection(User::All());
    }

    public function sendMail(Request $request){
        $details = [
            'title' => $request->subject,
            'message' => $request->message,
            'subject' => $request->subject,
            'email' => $request->email,
            'name' => $request->name,
        ];
        $mail = Mail::to('bbstryllogy@gmail.com')->send(new MailSupport($details));
    }
}
