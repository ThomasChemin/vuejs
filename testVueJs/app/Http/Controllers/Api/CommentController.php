<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Get all the comment
     * @return CommentResource collection
     */
    public function getAll(){
        return CommentResource::collection(Comment::all());
    }

    /**
     * Get a comment by his id
     * @return CommentResource
     */
    public function get($commentId){
        return new CommentResource(Comment::find($commentId));
    }

    /**
     * Get all the comment for the connected user
     * @return CommentResource collection
     */
    public function getAllFromUser(){
        return CommentResource::collection(Comment::where('idUser', '=', Auth::id())->orderBy('created_at', 'desc')->get());
    }

    /**
     * Get all the comment for a product
     */
    public function getAllFromProduct($productId){
        return CommentResource::collection(Comment::where('idProduct', '=', $productId)->orderBy('created_at', 'desc')->get());
    }

    /**
     * Get all the comment for a product that belongs to the connected user
     * @return CommentResource collection
     */
    public function getAllFromProductWithUser($productId){
        return CommentResource::collection(Comment::where('idProduct', '=', $productId)->where('idUser', '=', Auth::id())->orderBy('created_at', 'desc')->get());
    }

    /**
     * Add a comment
     * @return CommentResource
     */
    public function add(Request $request){
        return new CommentResource(Comment::create([
            'idUser' => Auth::id(),
            'idProduct' => $request->product,
            'comment' => $request->comment,
        ]));
    }

    /**
     * Remove a comment
     */
    public function remove(Request $request){
        error_log($request->commentId);
        Comment::find($request->commentId)->delete();
    }
}
