<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * get all the products
     * @return ProductRessource
     */
    public function index(){
        return ProductResource::collection(Product::all());
    }

    /**
     * get all product with a paginate 12
     * @return ProductRessource
     */
    public function allProductsPaginate12(){
        return ProductResource::collection(Product::paginate(12));
    }

    /**
     * get all product from a subCategory
     * @param categoryId id of category
     * @param subCategoryId id of subCategory
     * @return ProductResource
     */
    public function allProductFromCategory($categoryId, $subCategoryId){
        $listProducts = Product::join('product_sub_category', 'product.id', '=', 'product_sub_category.idProduct')
        ->join('sub_category', 'product_sub_category.idSubCategory', '=', 'sub_category.id')
        ->where('sub_category.id', '=', $subCategoryId)
        ->join('sub_category_category', 'sub_category.id', '=', 'sub_category_category.idSubCategory')
        ->join('category', 'sub_category_category.idCategory', '=', 'category.id')
        ->where('category.id', '=', $categoryId)
        ->select('product.*')
        ->orderBy('name')
        ->paginate(12);
        return ProductResource::collection($listProducts);
    }

    /**
     * Get a product by his ID
     * @return ProductResource
     */
    public function find($prodId){
        return new ProductResource(Product::find($prodId));
    }

    /**
     * Get all product starting bt a string
     * @return ProductResource
     */
    public function startingBy($name = null){
        return ProductResource::collection(Product::where('name', 'LIKE', $name."%")->orderBy('name')->paginate(12));
    }
}
