<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Basket;
use App\Models\Bill;
use App\Models\Product;
use App\Models\ProductVariation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class BillController extends Controller
{
    public function getAllFromUser($idUser)
    {
        return '';
    }

    /**
     * Get all the countries that Stripe support
     */
    public function retrieveCountriesForStripe(){
        $stripe = new \Stripe\StripeClient(
            'sk_test_51IU7keJGOckT26qB12lACfTP44NM57cduEbDcI5dgtPP9kQprukJajD04txxaxyegkM3Ils4BWOuChPMv6QYV5GD00VbsLW8Ro'
          );
          return ($stripe->countrySpecs->all())['data'];
    }

    /** 
     * Proceed to the payment
     * Create a new basket for the user
     * Create a bill of the basket
     * @return Bill
     */
    public function add(Request $request)
    {
        $carts = $request->input('cart');
        $user = Auth::user();
        try {
            $variationProducts = [];
            $canReduceAll = true;
            $variationError = new ProductVariation();
            foreach ($carts as $cart) {
                $variationProduct = ProductVariation::where('id', '=', $cart['variation']['id'])->first();
                if ((($variationProduct->stock) - ($cart['quantity'])) < 0) {
                    $variationError = $variationProduct;
                    $canReduceAll = false;
                } else {
                    $variationProducts[] = $variationProduct;
                    ProductVariation::where('id', '=', $cart['variation']['id'])->update(['stock' => ($variationProduct->stock - $cart['quantity'])]);
                }
            }
            if (!$canReduceAll) {
                foreach ($variationProducts as $variationProduct) {
                    ProductVariation::where('id', '=', $variationProduct->id)->update(['stock' => $variationProduct->stock]);
                }
                return response()->json(['message' => "Le produit ".$variationError->product->name." en taille : ".$variationError->variations['size']." et couleur : ".$variationError->variations['color']." est en quantité insuffisante."], 500);
            }
            $user->createOrGetStripeCustomer();
            $payment = $user->charge(
                $request->input('amount'),
                $request->input('payment_method_id')
            );
            $payment = $payment->asStripePaymentIntent();
            $bill = Bill::create([
                'idUser' => $user->id,
                'idBasketCompleted' =>  $carts[0]['basket']['id'],
                'price' => $payment->charges->data[0]->amount,
                'adresse' => $request->adresse,
            ]);
            Basket::create([
                'idUser' => $user->id,
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
        return $bill;
    }
}
