<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WishlistResource;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    /**
     * Get all the wishlist of the connected user
     * @return WishlistResource collection
     */
    public function getAllFromUser(){
        return WishlistResource::collection(Wishlist::where('idUser', '=', Auth::id())->get());
    }

    /**
     * Get all the wishlist from a user with pagiante 12
     * @return WishlistResource collection
     */
    public function getAllFromUserPaginateTwelve(){
        return WishlistResource::collection(Wishlist::where('idUser', '=', Auth::id())->paginate(12));
    }

    /**
     * Add a product to the wishlist of the connected user
     */
    public function add(Request $request){
        Wishlist::create([
            'idUser' => Auth::id(),
            'idProduct' => $request->productId,
        ]);
    }

    /**
     * Remove a product to the wishlist of the connected user
     */
    public function remove(Request $request){
        Wishlist::where('idUser', '=', Auth::id())->where('idProduct', '=', $request->productId)->delete();
    }

    /**
     * Get a wishlist's product for the connected user
     * @return Wishlist
     */
    public function getFromProductIdForUser($productId){
        return Wishlist::where('idUser', '=', Auth::id())->where('idProduct', '=', $productId)->first();
    }
}
