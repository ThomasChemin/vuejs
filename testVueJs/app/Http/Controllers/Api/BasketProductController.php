<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BasketProductResource;
use App\Models\Basket;
use App\Models\BasketProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketProductController extends Controller
{
    /**
     * Get all the data from basketProduct
     * @return BasketProductResource collection
     */
    public function getAll()
    {
        return BasketProductResource::collection(BasketProduct::limit(25)->get());
    }

    /**
     * Get all the data from a user from basketProduct 
     * @return BasketProductResource collection
     */
    public function getAllFromUser()
    {
        return BasketProductResource::collection(BasketProduct::where('idBasket', '=', (Basket::where('idUser', '=', Auth::id())->orderBy('created_at', 'DESC')->first())->id)
        ->get());
    }

    public function getById($idBasket){
        return BasketProductResource::collection(BasketProduct::where('idBasket', '=', $idBasket)->get());
    }

    /**
     * Add in basketProduct a product for the current user
     * If it already exist, increment the quantity by one
     * @return string
     */
    public function addProductToBasket(Request $request)
    {
        $basketProduct = BasketProduct::where('idBasket', '=', Auth::user()->baskets[Auth::user()->baskets->count() - 1 ]->id)->where('idProductVariation', '=', $request->prodId)->first();
        if ($basketProduct == null) {
            BasketProduct::create([
                'idBasket' => Auth::user()->baskets[Auth::user()->baskets->count() - 1]->id,
                'idProductVariation' => $request->prodId,
                'quantity' => 1,
            ]);
            return 'inserted';
        }
        BasketProduct::where('idBasket', '=', Auth::user()->baskets[Auth::user()->baskets->count() - 1]->id)->where('idProductVariation', '=', $request->prodId)->update(['quantity' => $basketProduct->quantity + 1]);
        return 'updated';
    }

    /**
     * Decrease or increase the quantity of a product in a basket. Depend on the user's action
     * If the quantity is already at 1 and the user reduce, we remove the product from the basket
     */
    public function update(Request $request){
        $basketProduct = BasketProduct::where('idBasket', '=', $request->basket)->where('idProductVariation', '=', $request->prodId)->first();
        $request->calcul == 'reduce' 
        ? BasketProduct::where('idBasket', '=', $request->basket)->where('idProductVariation', '=', $request->prodId)->update(['quantity' => $basketProduct->quantity -1 ]) 
        : BasketProduct::where('idBasket', '=', $request->basket)->where('idProductVariation', '=', $request->prodId)->update(['quantity' => $basketProduct->quantity + 1]);
    }

    /**
     * Delete a product from a basket
     */
    public function delete(Request $request){
        BasketProduct::where('idBasket', '=', $request->basket)->where('idProductVariation', '=', $request->prodId)->delete();
    }
}
