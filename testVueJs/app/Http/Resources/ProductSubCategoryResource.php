<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductSubCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (preg_match('/product/i', (explode('/', $request->path()))[1])) {
            return [
                'subCategory' => [
                    'id' => $this->subCategory->id,
                    'name' => $this->subCategory->name
                ],
            ];
        }
        if(preg_match('/subCategory/i', (explode('/', $request->path()))[1])) {
            return [
                'product' => [
                    'id' => $this->product->id,
                    'description' => $this->product->description,
                    'name' => $this->product->name,
                    'reference' => $this->product->reference,
                    'dimension' => $this->product->dimension,
                    'stock' => $this->product->stock,
                    'price' => $this->product->price / 100,
                    'sold' => $this->product->sold,
                    'imageAdresse' => $this->product->imageAdresse,
                    'idBrand' => new BrandResource($this->product->brand),
                ]
            ];
        }
    }
}
