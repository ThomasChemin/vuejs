<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'name' => $this->name,
            'reference' => $this->reference,
            'price' => $this->price / 100,
            'sold' => $this->sold,
            'imageAdresse' => $this->imageAdresse,
            'subCategories' => ProductSubCategoryResource::collection($this->subCategories),
            'variations' => ProductVariationResource::collection($this->productVariations),
            'idBrand' => new BrandResource($this->brand),
            'created_at' => $this->created_at->format('Y-m-d'),
        ];
    }
}
