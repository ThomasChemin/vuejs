<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BasketProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product' => [
                'id' => $this->productVariation->product->id,
                'name' => $this->productVariation->product->name,
                'reference' => $this->productVariation->product->reference,
                'price' => $this->productVariation->product->price / 100,
                'sold' => $this->productVariation->product->sold,
                'imageAdresse' => $this->productVariation->product->imageAdresse,
                'idBrand' => new BrandResource($this->productVariation->product->brand),
            ],
            'variation' => [
                'id' => $this->productVariation->id,
                'variations' => $this->productVariation->variations,
                'stock' => $this->productVariation->stock,
            ],
            'basket' => [
                'id' => $this->basket->id,
            ],
            'quantity' => $this->quantity,
        ];
    }
}
