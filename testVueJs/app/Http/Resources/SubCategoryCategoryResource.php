<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubCategoryCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        switch ((explode('/', $request->path()))[1]) {
            case 'allCategories':
                return [
                    'subCategory' => [
                        'id' => $this->subCategory->id,
                        'name' => $this->subCategory->name
                    ],
                ];
            case 'allSubCategories':
                return [
                    'category' => [
                        'id' => $this->category->id,
                        'name' => $this->category->name
                    ]
                ];
            default:
                break;
        }
    }
}
