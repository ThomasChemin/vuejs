import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    debug: true,
    state: {
        nbItemBasket: 0
    },
    mutations: {
        addItemCountToBasket(state) {
            state.nbItemBasket += 1;
        },
        removeItemCountToBasket(state) {
            state.nbItemBasket -= 1;
        },
        clearNbItemBasket(state) {
            state.nbItemBasket = 0;
        },
    },
    getters: {
        getNbItemBasket: state => {
            return state.nbItemBasket;
        }
    }
})