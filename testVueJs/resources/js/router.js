import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import Products from './components/Products'
import DetailProduct from './components/DetailProduct'
import Accueil from './components/Accueil'
import Basket from './components/Basket'
import Pay from './components/Pay'
import Bill from './components/Bill'
import NotFound from './components/NotFound'
import Nothing from './components/Nothing'

const routes = [{
        path: '/login',
        name: 'login',
        component: Nothing
    },
    {
        path: '/register',
        name: 'register',
        component: Nothing
    },
    {
        path: '/',
        name: 'home',
        component: Accueil
    },
    {
        path: '/accueil',
        name: 'accueil',
        component: Accueil
    },
    {
        path: '/productsByCat/:categoryId/:subCategoryId',
        name: 'productsByCat',
        component: Products
    },
    {
        path: '/productsByName/:name?',
        name: 'productsByName',
        component: Products
    },
    {
        path: '/wishlist',
        name: 'wishlist',
        component: Products
    },
    {
        path: '/detailProduct/:prodId',
        name: 'detailProduct',
        component: DetailProduct
    },
    {
        path: '/basket',
        name: 'basketOfUser',
        component: Basket
    },
    {
        path: '/proceedPay',
        name: 'pay',
        component: Pay
    },
    {
        path: '/bill',
        name: 'bill',
        component: Bill
    },
    {
        path: '*',
        component: NotFound
    },
]

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})