/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import router from './router';
import store from './store';
window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('accueil-vue', require('./components/Accueil.vue').default);
Vue.component('header-vue', require('./components/Header.vue').default);
Vue.component('footer-vue', require('./components/Footer.vue').default);
Vue.component('show-users', require('./components/Users.vue').default);
Vue.component('show-categories', require('./components/Categories.vue').default);
Vue.component('show-products', require('./components/Products.vue').default);
Vue.component('show-pay', require('./components/Pay.vue').default);
Vue.component('show-bill', require('./components/Bill.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('modal-not-connected', require('./components/ModalNotConnected.vue').default);
Vue.component('comments', require('./components/Comments.vue').default);
Vue.component('wishlist', require('./components/Wishlist.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

new Vue({
    el: '#app',
    router,
    store,
    props: {
        user: {
            type: Object,
            default: null,
        },
        bill: {
            type: Object,
            default: null,
        }
    },
});