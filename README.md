## Sommaire
1. [VueJS](#vuejs)
2. [Status](#status)
3. [Consomateur](#consomateur)
4. [Mise en place du projet](#mise-place-projet)

## VueJS
<a name="vuejs"></a>
Le projet VueJS est un site de ECommerce vestimentaire. Les utilisateurs peuvent voir les produits et les rechercher selon des filtres. Ils peuvent ajouter des vêtements à leur panier pour payer plus tard.
Le site utilise le système de payement Stripe. Il se base sur le framework php Laravel, css Bootstrap, et javascript VueJS
***
## Status
<a name="status"></a>
En cours
***
## Consomateur
<a name="consomateur"></a>
Un visiteur peut s'enregistrer et se connecter via les formulaires fournit par Laravel.
Il peut ajouter ajouter des produits à son panier seulement s'il est connecté sinon, un modal apparait pour lui dire de se connecter.
Il peut augmenter la quantité d'un produit qu'il a dans son panier. Choisir les variations qu'il veut (taille, couleur).
Il peut enfin payer pour passer la commande.
***
## Mise en place du projet
<a name="mise-place-projet"></a>
Une fois le projet cloné, créer un .env en suivant l'example du .env.example. Remplacer les champs avec vos informations. (Base de données, email...)

Il faut rajouter 2 lignes : 

> STRIPE_KEY="your-stripe-key"

> STRIPE_SECRET="your-stripe-secret"

Vous trouverez ces clés sur votre compte Stripe dans l'onglet "Développeur".

Une fois le .env créer faite la commande suivante dans la racine de votre projet : 

> ./artisan key:generate

Pour générer la base de données, faites les commandes suivantes dans la racine de votre projet. Pour générer les tables : 

>./artisan migrate:fresh 

Enfin, tapez la commande suivante pour générer des données :

>./artisan db:seed 

Les données ne sont en aucun cas explicites. Elles sont juste présentes pour donner de la matière au site.
***

